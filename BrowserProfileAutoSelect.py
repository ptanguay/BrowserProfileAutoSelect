from json import load as load_json
from subprocess import Popen, PIPE
from os.path import expanduser, join
from sys import argv

try:
    url = argv[1]
except IndexError:
    url = ""

with open(join(expanduser("~"), "BrowserProfileAutoSelect.json")) as f:
    config = load_json(f)

profile = config['default_profile'] or 'Default'
del config['default_profile']

if url:
    for k, v in config.iteritems():
        if k in url:
            profile = v

CREATE_NEW_PROCESS_GROUP = 0x00000200
DETACHED_PROCESS = 0x00000008

p = Popen(['C:\Program Files (x86)\Google\Chrome\Application\chrome.exe',
           '--profile-directory=%s' % profile,
           '--new-tab',
           url],
          shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE,
          creationflags=DETACHED_PROCESS | CREATE_NEW_PROCESS_GROUP)
